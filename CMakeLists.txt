cmake_minimum_required(VERSION 3.5)
project(skillset_monitoring)

# Default to C99
if(NOT CMAKE_C_STANDARD)
  set(CMAKE_C_STANDARD 99)
endif()
# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()
if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

message(STATUS "CMake compiling package for ROS $ENV{ROS_DISTRO}")

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  message_generation
  diagnostic_msgs
  std_msgs
)

#catkin_python_setup()

## Generate messages in the 'msg' folder
add_message_files(
  FILES
  GoalStatus.msg
  SkillStatus.msg
  ResourceStatus.msg
  SkillsetStatus.msg
)

## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs
)

catkin_package(
  #  INCLUDE_DIRS include
  #  LIBRARIES robot_skills_introspection
  CATKIN_DEPENDS message_runtime diagnostic_msgs std_msgs
  #  DEPENDS system_lib
)
