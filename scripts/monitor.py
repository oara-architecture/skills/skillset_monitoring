#!/usr/bin/env python3
import rospy
import rosservice
import roslib.message

from std_msgs.msg import Header
from diagnostic_msgs.srv import SelfTest
from skillset_monitoring.msg import SkillStatus, ResourceStatus, GoalStatus, SkillsetStatus

def get_active_services(known_srvs, ns):
    new_skills, new_resources = [], []
    for srv_name in rosservice.get_service_list():
        rospy.logdebug(f"Found service {srv_name}")
        if srv_name in known_srvs:
            pass
        elif srv_name.startswith(ns):
            try:
                srv_type = rosservice.get_service_type(srv_name)
                if srv_type == 'diagnostic_msgs/SelfTest':
                    rospy.loginfo("Found new skill service {}".format(srv_name))
                    new_skills.append((srv_name, SelfTest))
                if srv_type.endswith('/State'):
                    rospy.loginfo("Found new resource service {}".format(srv_name))
                    new_resources.append((srv_name, srv_type))
            except rosservice.ROSServiceIOException:
                rospy.logdebug(f"Error communicating with service {srv_name}")
            except Exception as ex:
                rospy.logwarn(f"Error when getting information for service {srv_name}: {ex}")
    return new_skills, new_resources


class SkillsetMonitor:
    def __init__(self):
        self.__pub = rospy.Publisher('skillset_status', SkillsetStatus, queue_size=10)
        self.__msg = SkillsetStatus()
        self.__msg.header = Header()
        self.__msg.header.frame_id = rospy.get_namespace()
        self.__skills = dict()
        self.__resources = dict()

        self.look_for_skillset_managers(None)
        self.__loop_timer = rospy.Timer(rospy.Duration(1.0), self.gather_status)
        self.__look_timer = rospy.Timer(rospy.Duration(10.0), self.look_for_skillset_managers)

    def look_for_skillset_managers(self, event):
        for srv in list(self.__skills):
            try:
                rospy.wait_for_service(srv, timeout=0.01)
            except rospy.ROSException:
                del self.__skills[srv]
        for srv in list(self.__resources):
            try:
                rospy.wait_for_service(srv, timeout=0.01)
            except rospy.ROSException:
                del self.__resources[srv]

        new_skills, new_resources = get_active_services(list(self.__skills.keys()) + list(self.__resources.keys()), rospy.get_namespace())
        for (s, t) in new_skills:
            self.__skills[s] = rospy.ServiceProxy(s, t)
        for (s, t) in new_resources:
            srv = roslib.message.get_service_class(t)
            self.__resources[s] = rospy.ServiceProxy(s, srv)

    def gather_status(self, event):
        self.__msg.header.stamp = rospy.Time.now()
        del self.__msg.skills[:]
        del self.__msg.resources[:]

        for srv, proxy in self.__skills.items():
            rospy.logdebug("Getting status of skill {}".format(srv))
            try:
                info = proxy()
            except rospy.service.ServiceException:
                rospy.logwarn("Error when calling status service {}".format(srv))
                continue

            skill_msg = SkillStatus()
            for st in info.status:
                if (not st.hardware_id) and st.message:
                    skill_msg.name = st.name
                    skill_msg.input_type = st.message
                else:
                    goal_msg = GoalStatus()
                    goal_msg.id = st.hardware_id
                    for kv in st.values:
                        if kv.key == 'goal':
                            goal_msg.input_value = kv.value
                        if kv.key == 'start_time':
                            goal_msg.start_time = float(kv.value)
                        if kv.key == 'end_time':
                            goal_msg.termination_time = float(kv.value)
                        if kv.key == 'progress':
                            try:
                                goal_msg.progress = float(kv.value)
                            except:
                                goal_msg.progress = 0.0
                        if kv.key == 'skill_state':
                            goal_msg.state = kv.value
                        if kv.key == 'skill_state_text':
                            goal_msg.msg = kv.value
                    skill_msg.goals.append(goal_msg)

            self.__msg.skills.append(skill_msg)

        for srv, proxy in self.__resources.items():
            try:
                info = proxy()
            except rospy.service.ServiceException:
                rospy.logwarn("Error when calling status service {}".format(srv))
                continue

            res_msg = ResourceStatus()
            if '/get_state' in srv: # prev version of managers
                i = srv.split('/')
                res_msg.name = i[-2]
            else:
                pattern = 'resource/get_'
                i = srv.find(pattern) + len(pattern)
                res_msg.name = srv[i:]
            res_msg.state = info.state
            self.__msg.resources.append(res_msg)

        rospy.logdebug(self.__msg)
        self.__pub.publish(self.__msg)


def main():
    rospy.init_node("skillset_monitor", log_level=rospy.DEBUG)
    monitor = SkillsetMonitor()
    rospy.spin()

if __name__ == '__main__':
    main()
